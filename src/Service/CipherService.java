package Service;

import UserApp.Controller;
import UserApp.UserController;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import javax.crypto.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class CipherService {
    private static PublicKey caPublicKey;
    private static Key rsaPrivateKey;
    private static Key dsaPrivateKey;
    private static String pathToCode="";
    static{
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate caCert = cf.generateCertificate(new FileInputStream("CA_tijelo/certs/CA_CodeObfuscatorCert.cer"));
            caPublicKey = caCert.getPublicKey();
        }catch (Exception e)
        {
            Controller.alertFunc("CAerror","No CA certificate!", Alert.AlertType.ERROR, ButtonType.OK);
            System.exit(1);
        }
    }
    public static void encrypt(String path,String username,String sendUsername) throws NoSuchAlgorithmException, IOException, CertificateException, SignatureException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, BadPaddingException, NoSuchProviderException {
        //read RSA public key from cert
        UserController.updateEncryptText("Start encryption process..."+System.lineSeparator());
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) cf.generateCertificate(
                new FileInputStream("UsersCert/" + sendUsername+"CertAut.cer"));
        UserController.updateEncryptText("Check validity..."+System.lineSeparator());
        cert.checkValidity();
        cert.verify(caPublicKey);
        PublicKey rsaPublicKey=cert.getPublicKey();
        //set rsapubkey to crypt aes key
        Cipher rsaKey = Cipher.getInstance("RSA");
        rsaKey.init(Cipher.WRAP_MODE, rsaPublicKey);
        //generate aes session key
        KeyGenerator keyAes = KeyGenerator.getInstance("AES");
        keyAes.init(128);
        SecretKey generatedKey = keyAes.generateKey();
        byte[] cryptAesKey=rsaKey.wrap(generatedKey);//crypt aes key
        //encrypt file
        Cipher symmetricKeyAes = Cipher.getInstance("AES/ECB/PKCS5Padding");
        symmetricKeyAes.init(Cipher.ENCRYPT_MODE, generatedKey);
        File fileToEncrypt=new File(path);
        UserController.updateEncryptText("Loading file..."+System.lineSeparator());
        byte[] cryptFile= Files.readAllBytes(fileToEncrypt.toPath());
        UserController.updateEncryptText("Start encryption file.."+System.lineSeparator());
        byte[] encryptedFile=symmetricKeyAes.doFinal(cryptFile);
        UserController.updateEncryptText("End process encryption file..."+System.lineSeparator());
        String nameCryptFile=Base64.getUrlEncoder().encodeToString(symmetricKeyAes.doFinal(fileToEncrypt.getName().getBytes()));
        UserController.updateEncryptText("Write encryption file..."+System.lineSeparator());
        FileOutputStream out = new FileOutputStream( "Users/"+sendUsername+"/inbox/"+username+"/"+nameCryptFile+".xyz");
        out.write(encryptedFile);
        out.close();
        //Write info to file
        rsaKey.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
        File file= new File("Users/"+sendUsername+"/inbox/"+username+"/"+nameCryptFile+".sign");
        PrintWriter pw=new PrintWriter(file);
        pw.print(Base64.getUrlEncoder().encodeToString(rsaKey.doFinal(username.getBytes()))+":"+Base64.getUrlEncoder().encodeToString(rsaKey.doFinal(sendUsername.getBytes()))+":"+Base64.getEncoder().encodeToString(cryptAesKey)+":"+getSignatureOfFile(cryptFile));
        pw.close();
        byte[] fileSign=Files.readAllBytes(Paths.get("Users/"+sendUsername+"/inbox/"+username+"/"+nameCryptFile+".sign"));
        //signature
        Signature ds = Signature.getInstance("SHA256withDSA");
        ds.initSign((PrivateKey) dsaPrivateKey);
        ds.update(fileSign);
        byte[] fileHash = ds.sign();
        File fileSignature= new File("Users/"+sendUsername+"/inbox/"+username+"/"+nameCryptFile+".signature");
        FileOutputStream fout=new FileOutputStream(fileSignature);
        fout.write(fileHash);
        fout.close();
        UserController.updateEncryptText("Encryption success done..."+System.lineSeparator());
        setHashInbox(sendUsername);
    }
    private static String getSignatureOfFile(byte[] encrypedFile) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        return Base64.getEncoder().encodeToString(md.digest(encrypedFile));
    }
    public static void decrypt(String path,String username) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException, BadPaddingException, IllegalBlockSizeException, CertificateException, SignatureException {
        //set rsa param
        UserController.updateDecryptText("Start decryption process..."+System.lineSeparator());
        Cipher rsaDec = Cipher.getInstance("RSA");
        Cipher rsaDecName=Cipher.getInstance("RSA");
        rsaDec.init(Cipher.UNWRAP_MODE,rsaPrivateKey);
        rsaDecName.init(Cipher.DECRYPT_MODE,rsaPrivateKey);
        //read aes crypt key and name of file
        File fileEnc= new File(path);
        String nameFile=fileEnc.getName().replace(".xyz","");
        String pathFile=path.replace(".xyz","");
        File fileSign=new File(pathFile+".sign");
        BufferedReader br = new BufferedReader(new FileReader(fileSign));
        String[] dateFromFile=br.readLine().split(":");
        br.close();
        String usernameFromFile=new String(rsaDecName.doFinal(Base64.getUrlDecoder().decode(dateFromFile[0])));
        String sendUsernameFromFile= new String(rsaDecName.doFinal(Base64.getUrlDecoder().decode(dateFromFile[1])));
        if(sendUsernameFromFile.equals(username)) {
            byte[] symmeticAesKey = Base64.getDecoder().decode(dateFromFile[2]);
            //decrypt aes with rsa private key
            Key decryptKeyAes = rsaDec.unwrap(symmeticAesKey, "AES", Cipher.SECRET_KEY);
            Cipher symmetricAesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            symmetricAesCipher.init(Cipher.DECRYPT_MODE, decryptKeyAes);
            UserController.updateDecryptText("Loading file..." + System.lineSeparator());
            byte[] cryptFile = Files.readAllBytes(Paths.get(path));
            //verify file
            UserController.updateDecryptText("Starting process verify..." + System.lineSeparator());
            Signature ds = Signature.getInstance("SHA256withDSA");
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate x509cert = (X509Certificate) cf.generateCertificate(
                    new FileInputStream("UsersCert/" + usernameFromFile + "Cert.cer"));

            ds.initVerify(x509cert);
            ds.update(Files.readAllBytes(fileSign.toPath()));
            boolean isVerify = ds.verify(Files.readAllBytes(Paths.get(pathFile + ".signature")));
            if (isVerify) {
                UserController.updateDecryptText("Verify succesfull..." + System.lineSeparator());
                UserController.updateDecryptText("Start decrypting file..." + System.lineSeparator());
                byte[] decrypt = symmetricAesCipher.doFinal(cryptFile);
                byte[] decryptNameFile = symmetricAesCipher.doFinal(Base64.getUrlDecoder().decode(nameFile));
                String hashFile = getSignatureOfFile(decrypt);
                if (hashFile.equals(dateFromFile[3])) {
                    UserController.updateDecryptText("Writting file..." + System.lineSeparator());
                    File file = new File(pathFile.replace(nameFile, "") + new String(decryptNameFile));
                    PrintWriter pw = new PrintWriter(file);
                    pw.println(new String(decrypt));
                    pw.close();
                    UserController.updateDecryptText("Decryption done succesfull" + System.lineSeparator());
                    pathToCode=file.getAbsolutePath();
                    setHashInbox(username);
                } else UserController.updateDecryptText("File is attacked..." + System.lineSeparator());
            } else UserController.updateDecryptText("Verify failded..." + System.lineSeparator());
        }else UserController.updateDecryptText("Attack on user..." + System.lineSeparator());
    }
    public static boolean isLoggedIn(String username,String passwd,String pathAutCert,String pathSignCert) throws IOException, NoSuchAlgorithmException, UnrecoverableKeyException, InvalidKeyException, KeyStoreException, NoSuchProviderException, CertificateException, SignatureException, DeletedFileException {
        File directory=new File("Users");
        if(!verifyHashInbox(username)){
            throw new DeletedFileException("Attack on file! Deleted message file!");
        }
        File[] listFiles=directory.listFiles();
        for(File fileName:listFiles){
            if(fileName.getName().equals(username)){
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    String passwdHash = Base64.getEncoder().encodeToString(md.digest(passwd.getBytes()));
                    File file= new File("Users/"+username+"/"+username+".txt");
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String[] dateFromFile=br.readLine().split(":");
                    if(dateFromFile[0].equals(username) && dateFromFile[1].equals(passwdHash))
                    {
                        KeyStore ks=KeyStore.getInstance("PKCS12");
                        ks.load(new FileInputStream(pathSignCert),passwd.toCharArray());
                        Certificate ds=ks.getCertificate("1");
                        ds.verify(caPublicKey);
                        dsaPrivateKey=ks.getKey("1",passwd.toCharArray());
                        ks.load(new FileInputStream(pathAutCert),passwd.toCharArray());
                        Certificate autDS=ks.getCertificate("1");
                        autDS.verify(caPublicKey);
                        rsaPrivateKey=ks.getKey("1",passwd.toCharArray());
                        return true;
                    }
                    else return false;
            }
        }
        return false;
    }
    private static void  compileAndRunCFile(String fileName) {
        String compileFileCommand = "gcc " + "\""+fileName +"\"" +" -o "+ "\"" + fileName.replace(".c",".exe")+ "\"";
        String runFileCommand =  "\"" + fileName.replace(".c",".exe")+ "\"";
        try {
            int k =  runProcess(compileFileCommand);
            if (k==0) {
                k = runProcess(runFileCommand);
            }
        } catch (Exception e) {
            Controller.alertFunc("Error","Error running program!", Alert.AlertType.ERROR,ButtonType.CLOSE);
        }
    }
    private static void  compileAndRunCPPFile(String fileName) {
        String compileFileCommand = "g++ " + "\""+fileName +"\"" +" -o "+ "\"" + fileName.replace(".c",".exe")+ "\"";
        String runFileCommand =  "\"" + fileName.replace(".c",".exe")+ "\"";
        try {
            int k =  runProcess(compileFileCommand);
            if (k==0) {
                k = runProcess(runFileCommand);
            }
        } catch (Exception e) {
            Controller.alertFunc("Error","Error running program!", Alert.AlertType.ERROR,ButtonType.CLOSE);
        }
    }
    private static void  compileAndRunJavaFile(String fileName) {
        File fileJava=new File(fileName);
        String compileFileCommand = "javac " + "\""+fileName + "\"";
        String runFileCommand = "java " +"-classpath " + "\"" + fileName.replace("\\"+fileJava.getName(),"")+ "\"" +" " + fileJava.getName().replace(".java","");
        try {
            int k =  runProcess(compileFileCommand);
            if (k==0) {
                runProcess(runFileCommand);
            }
        } catch (Exception e) {
            Controller.alertFunc("Error","Error running program!", Alert.AlertType.ERROR,ButtonType.CLOSE);
        }
    }
    private static int runProcess(String command) throws Exception {
        Process pro = Runtime.getRuntime().exec(command);
        pro.waitFor();
        BufferedReader in = new BufferedReader(new InputStreamReader(pro.getInputStream()));
        BufferedReader inError = new BufferedReader(new InputStreamReader(pro.getErrorStream()));
        String line;
        while ((line = in.readLine()) != null) {
            UserController.updateRunText(line);
        }
        while ((line = inError.readLine()) != null) {
            UserController.updateRunText(line);
        }
        return pro.exitValue();
    }
    public static boolean compileAndRunProgram(){
        if(pathToCode.length() !=0){
            if(pathToCode.endsWith(".c")){
                compileAndRunCFile(pathToCode);
                return true;
            }
            else if(pathToCode.endsWith(".cpp")){
                compileAndRunCPPFile(pathToCode);
                return true;
            }
            else if(pathToCode.endsWith(".java")) {
                compileAndRunJavaFile(pathToCode);
                return true;
            }
            else return false;
        }
        return false;
    }
    private static byte[] getHashInbox(String username) throws NoSuchAlgorithmException, FileNotFoundException {
        byte[] hash=new byte[64];
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        File directory=new File("Users/"+username+"/inbox");
        String[] listInbox=directory.list();
        for(String listFile:listInbox){
            File sendUserFiles=new File("Users/"+username+"/inbox/"+listFile);
            String files[]=sendUserFiles.list();
            for(String file:files){
                byte[] hashFileName=md.digest(file.getBytes());
                for(int i=0;i<hashFileName.length;i++)
                    hash[i]^=hashFileName[i];
            }
        }
        return hash;
//        //signature
//        Signature ds = Signature.getInstance("SHA256withDSA");
//        ds.initSign((PrivateKey) dsaPrivateKey);
//        ds.update(fileSign);
//        byte[] fileHash = ds.sign();
//        File fileSignature= new File("Users/"+sendUsername+"/inbox/"+username+"/"+nameCryptFile+".signature");
//        FileOutputStream fout=new FileOutputStream(fileSignature);
//        fout.write(fileHash);
//        fout.close();
    }
    private static boolean verifyHashInbox(String username) throws IOException, NoSuchAlgorithmException {
        File fileSign=new File("dirHash/" +username+ ".txt");
        BufferedReader br = new BufferedReader(new FileReader(fileSign));
        String dateFromFile=br.readLine();
        br.close();
        byte[] hashDir=getHashInbox(username);
        if(dateFromFile.equals(Base64.getEncoder().encodeToString(hashDir))) return true;
        return false;
    }
    public static void setHashInbox(String username) throws FileNotFoundException, NoSuchAlgorithmException {
        PrintWriter pr=new PrintWriter(new File("dirHash/"+username+".txt"));
        pr.print(Base64.getEncoder().encodeToString(getHashInbox(username)));
        pr.close();
    }
}
