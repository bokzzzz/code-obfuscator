package Service;

public class DeletedFileException extends Exception {
    public DeletedFileException(String message){
        super(message);
    }
}
