package UserApp;

import Service.CipherService;
import Service.DeletedFileException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    static Stage stage;
    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button loginButton;

    @FXML
    private TextField AutCertField;

    @FXML
    private TextField CertForSingField;
    @FXML
    private ImageView imageView;
    @Override
    public void initialize(URL url, ResourceBundle resources) {
        imageView.setImage(new Image("icon.png"));
    }

    @FXML
    void login() {
        try {
            if (usernameField.getText().length() != 0 && passwordField.getText().length() != 0
                    && AutCertField.getText().length() != 0 && CertForSingField.getText().length() != 0) {
                if (CipherService.isLoggedIn(usernameField.getText(), passwordField.getText(),AutCertField.getText(),CertForSingField.getText())) {
                    try {
                        UserController.username=usernameField.getText();
                        loadFXML();
                    } catch (Exception e) {
                        alertFunc("Application error!","Error, file of application is missing!",Alert.AlertType.ERROR,ButtonType.CLOSE);
                    }
                } else {
                    alertFunc("Info","Wrong username or password!",Alert.AlertType.INFORMATION, ButtonType.OK);
                }
                }else{
                alertFunc("Some field is empty!","Set field!",Alert.AlertType.ERROR, ButtonType.OK);
                }
            }catch (InvalidKeyException e){
            alertFunc("Invalid key","",Alert.AlertType.ERROR, ButtonType.OK);
            }catch (NoSuchProviderException | CertificateException | SignatureException | KeyStoreException | EOFException e){
            alertFunc("Invalid certificate!","Please, set valid certicate!",Alert.AlertType.ERROR, ButtonType.OK);
            }catch (UnrecoverableKeyException | IOException e){
            alertFunc("Invalid certificate!","Please, set your certificate!",Alert.AlertType.ERROR, ButtonType.OK);
            }catch(DeletedFileException e){
            alertFunc("Error!!!",e.getMessage()+" Please, contact administrator!",Alert.AlertType.ERROR, ButtonType.OK);
            } catch(Exception e){
            alertFunc("Error","Unexpected error",Alert.AlertType.ERROR, ButtonType.OK);
            }

    }
    private void loadFXML() throws IOException {
            Parent root = FXMLLoader.load(getClass().getResource("UserFX.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage) loginButton.getScene().getWindow();
            UserController.stage=stage;
            stage.setScene(scene);
            stage.setTitle("Code Obfuscator");
        stage.setOnCloseRequest(windowEvent -> {
            try {
                CipherService.setHashInbox(usernameField.getText());
            }catch(Exception ex){
                alertFunc("Error","Unexpected error!",Alert.AlertType.ERROR, ButtonType.OK);
            }
        });

        stage.show();
    }
    @FXML
    private void openFile(ActionEvent eventAc){
        String buttonClick=((Button)eventAc.getSource()).getId();
        FileChooser fileChsr = new FileChooser();
        File dirInit=new File("Users/"+usernameField.getText());
        if(!dirInit.exists() || usernameField.getText().length() == 0)
        fileChsr.setInitialDirectory(new File("Users"));
        else fileChsr.setInitialDirectory(dirInit);
        if(buttonClick.equals("chooseButton1")) {
            fileChsr.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Certificate", "*Aut.pfx"));
        }
        else fileChsr.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Certificate", "*Sign.pfx"));
        File file = fileChsr.showOpenDialog(stage);
        if (file != null) {
            if(buttonClick.equals("chooseButton1"))
                AutCertField.setText(file.getAbsolutePath());
            else CertForSingField.setText(file.getAbsolutePath());
        }
    }
    @FXML
   public static void alertFunc(String title, String contentText, Alert.AlertType type, ButtonType btType) {
        Alert alert = new Alert(type, contentText, btType);
        alert.setTitle(title);
        alert.show();
    }
}
