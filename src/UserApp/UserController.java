package UserApp;

import Service.CipherService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.ResourceBundle;

public class UserController implements Initializable {
    static Stage stage;
    static String username;
    @FXML
    private AnchorPane pane;

    @FXML
    private TextField encryptFilePath;

    @FXML
    private static TextArea encryptTextArea;

    @FXML
    private static TextArea decryptTextArea;

    @FXML
    private static TextArea runTextArea;

    @FXML
    private TextField decryptFilePath;

    @FXML
    private ComboBox<String> comboBoxUsers;

    @Override
    public void initialize(URL url, ResourceBundle resources){
        File files=new File("Users");
        String[] users=files.list();
        for(String user:users)
            if(!user.equals(username))
                comboBoxUsers.getItems().add(user);
        encryptTextArea=new TextArea();
        encryptTextArea.setLayoutX(286);
        encryptTextArea.setLayoutY(14);
        encryptTextArea.setPrefWidth(200);
        encryptTextArea.setPrefHeight(171);
        encryptTextArea.setText("");
        encryptTextArea.setEditable(false);
        pane.getChildren().addAll(encryptTextArea);
        decryptTextArea=new TextArea();
        decryptTextArea.setLayoutX(286);
        decryptTextArea.setLayoutY(215);
        decryptTextArea.setPrefWidth(200);
        decryptTextArea.setPrefHeight(171);
        decryptTextArea.setText("");
        decryptTextArea.setWrapText(true);
        decryptTextArea.setEditable(false);
        pane.getChildren().addAll(decryptTextArea);
        runTextArea=new TextArea();
        runTextArea.setLayoutX(77);
        runTextArea.setLayoutY(310);
        runTextArea.setPrefWidth(183);
        runTextArea.setPrefHeight(76);
        runTextArea.setText("");
        runTextArea.setWrapText(true);
        runTextArea.setEditable(false);
        pane.getChildren().addAll(runTextArea);

    }
    @FXML
    private void openFile(ActionEvent eventAc){
        String buttonClick=((Button)eventAc.getSource()).getId();
        FileChooser fileChsr = new FileChooser();
        if(buttonClick.equals("encrOpenButton")) {
            fileChsr.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("c file", "*.c"),new FileChooser.ExtensionFilter("c++ file", "*.cpp"),new FileChooser.ExtensionFilter("java file", "*.java"));
        }
        else{
            fileChsr.setInitialDirectory(new File("Users/"+username+"/inbox"));
            fileChsr.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("decrypt file", "*.xyz"));
        }
        File file = fileChsr.showOpenDialog(stage);
        if (file != null) {
            if(buttonClick.equals("encrOpenButton"))
               encryptFilePath.setText(file.getAbsolutePath());
            else decryptFilePath.setText(file.getAbsolutePath());
        }
    }
    @FXML
    private void encryption(){
        encryptTextArea.setText("");
        if(encryptFilePath.getText().length() != 0 && comboBoxUsers.getSelectionModel().getSelectedItem() != null) {
            try {
                CipherService.encrypt(encryptFilePath.getText(), username, comboBoxUsers.getSelectionModel().getSelectedItem());
            } catch (IllegalBlockSizeException e) {
                updateEncryptText("Generate key failed");
            } catch (InvalidKeyException e) {
                updateEncryptText("Invalid key");
            } catch (NoSuchPaddingException e) {
                updateEncryptText("Get instance failded");
            } catch (NoSuchAlgorithmException e) {
                updateEncryptText("Algorithm not supported");
            } catch (IOException e) {
                updateEncryptText("Deleted selected file");
            } catch (BadPaddingException e) {
                updateEncryptText("Bad padding in file");
            } catch (CertificateException e) {
                updateEncryptText("Invalid user certificate");
            } catch (SignatureException e) {
                updateEncryptText("Signature error");
            } catch (NoSuchProviderException e) {
                updateEncryptText("Error!");
            }
        }
        else
        {
            Controller.alertFunc("Field empty!","Please,fill field ", Alert.AlertType.WARNING,ButtonType.OK);
        }
    }
    @FXML
    private void decryption(){
        decryptTextArea.setText("");
        if(decryptFilePath.getText().length() !=0) {
            try {
                CipherService.decrypt(decryptFilePath.getText(), username);
            } catch (NoSuchPaddingException e) {
                updateDecryptText("Attack on file");
            } catch (NoSuchAlgorithmException | InvalidKeyException e) {
                updateDecryptText("Attack on key");
            } catch (IOException e) {
                updateDecryptText("Attack on  file");
            } catch (BadPaddingException | IllegalBlockSizeException e) {
                updateDecryptText("Attack on config file");
            } catch (CertificateException e) {
                updateDecryptText("Attack on certificate file");
            } catch (SignatureException e) {
                updateDecryptText("Attack on signature info");
            }
        }
        else
        {
            Controller.alertFunc("File not choosed!","Please, choose file!", Alert.AlertType.WARNING,ButtonType.OK);
        }
    }
    @FXML
    private  void runProgram(){
        if(!CipherService.compileAndRunProgram()){
            Controller.alertFunc("Error","Not c or cpp file!", Alert.AlertType.ERROR,ButtonType.OK);
        }
    }
    public static void updateRunText(String text){
        runTextArea.appendText(text);
    }
    public static void updateEncryptText(String text){
        encryptTextArea.appendText(text);
    }
    public static void updateDecryptText(String text){
        decryptTextArea.appendText(text);
    }

}
