module CodeObfuscator {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    opens UserApp;
    opens Service;
}